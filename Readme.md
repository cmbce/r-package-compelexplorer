# General description
A companion package of the [CompEl](https://bitbucket.org/correlmetriquepaysageetbioag/compel/src/master/) elicitation tool. Allows to easily import and analyze the json output. 

# Set up

1. clone 
2. launch R in CompelExplorer/
3. In R: 

``` 
        library("devtools")
        document()
        install()
```

